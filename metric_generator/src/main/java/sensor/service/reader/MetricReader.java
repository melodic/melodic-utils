package sensor.service.reader;

public interface MetricReader {

    void readMetrics() throws Exception;
}

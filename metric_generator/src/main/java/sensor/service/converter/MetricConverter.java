package sensor.service.converter;

import sensor.common.pojo.MetricPOJO;

public interface MetricConverter {

    String convert(MetricPOJO metricPOJO);

}

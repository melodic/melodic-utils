package sensor.service.reader;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import sensor.common.pojo.MetricPOJO;
import sensor.service.MockitoExtension;
import sensor.service.chooser.FileChooser;
import sensor.service.parser.LogParser;
import sensor.service.sender.MetricSender;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FileMetricReaderTest {

    @Test
    @DisplayName("Should return if no files is found")
    void shouldReturnIfNoFilesIsFound(@Mock LogParser logParser, @Mock MetricSender metricSender, @Mock FileChooser fileChooser) throws Exception {
        //given
        when(fileChooser.findFilesToAnalise()).thenReturn(Collections.emptyMap());
        FileMetricReader fileMetricReader = new FileMetricReader(logParser, metricSender, fileChooser);

        //when
        fileMetricReader.readMetrics();

        //then
        verify(fileChooser, times(1)).findFilesToAnalise();
    }

    @Test
    @DisplayName("Should work with one file and no metric is created")
    void shouldWorkWithOneFileWhenNoMetricsIsCreated(@Mock LogParser logParser, @Mock MetricSender metricSender, @Mock FileChooser fileChooser) throws Exception {
        //given
        Path path = Paths.get(getClass().getResource("testFile_1.txt").toURI());

        when(fileChooser.findFilesToAnalise()).thenReturn(Collections.singletonMap(path, 1));

        when(logParser.parseLineToPOJO(anyString())).thenReturn(null);

        FileMetricReader fileMetricReader = new FileMetricReader(logParser, metricSender, fileChooser);

        //when
        fileMetricReader.readMetrics();

        //then
        verify(fileChooser, times(1)).findFilesToAnalise();
        verify(fileChooser, times(1)).updateFileIndex(anyString(), anyInt());
    }

    @Test
    @DisplayName("Should work with one file and 4 metrics are created")
    void shouldWorkWithOneFileWhenFourMetricsAreCreated(@Mock LogParser logParser, @Mock MetricSender metricSender, @Mock FileChooser fileChooser) throws Exception {
        //given
        Path path = Paths.get(getClass().getResource("testFile_1.txt").toURI());

        when(fileChooser.findFilesToAnalise()).thenReturn(Collections.singletonMap(path, 1));

        when(logParser.parseLineToPOJO(any())).thenReturn(new MetricPOJO("1", 2.0, 3));

        doNothing().when(metricSender).sendMessage(any());

        FileMetricReader fileMetricReader = new FileMetricReader(logParser, metricSender, fileChooser);

        //when
        fileMetricReader.readMetrics();

        //then
        verify(fileChooser, times(1)).findFilesToAnalise();
        verify(fileChooser, times(5)).updateFileIndex(anyString(), anyInt());

        verify(logParser, times(4)).parseLineToPOJO(any());

        verify(metricSender, times(4)).sendMessage(any());
    }

//    @Override
//    public void readMetrics() throws Exception {
//        Map<Path, Integer> filesToAnalise = fileChooser.findFilesToAnalise();
//
//        for (Path path : filesToAnalise.keySet()) {
//            Integer lineNumber = filesToAnalise.get(path);
//            IndexedFileReader reader = new IndexedFileReader(path.toFile());
//            int linesCount = reader.getLineCount();
//
//            log.info("Reading file: {} started", path.toString());
//
//            for (Map.Entry<Integer, String> entry : reader.readLines(lineNumber, linesCount).entrySet()) {
//                MetricPOJO metricPOJO = logParser.parseLineToPOJO(entry.getValue());
//                if (metricPOJO != null) {
//                    metricSender.sendMessage(metricPOJO);
//                    fileChooser.updateFileIndex(path.toString(), entry.getKey());
//                }
//            }
//            fileChooser.updateFileIndex(path.toString(), linesCount);
//
//            log.info("Reading file: {} finished after reading {} lines ({} - {})", path.toString(), linesCount - lineNumber, lineNumber, linesCount);
//        }
//    }



}
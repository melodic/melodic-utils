package sensor.service.parser;

import org.junit.jupiter.api.*;
import sensor.common.pojo.MetricPOJO;

import static org.junit.jupiter.api.Assertions.*;

class FcrLogParserTest {

    private static FcrLogParser fcrLogParser;

    @BeforeAll
    static void setup(){
        fcrLogParser = new FcrLogParser("metricName");
    }

    @Test
    @DisplayName("Method shoud return null if line is null, blank, etc.")
    void shouldReturnNullIfLineIsEmpty(){

        assertAll(() -> assertNull(fcrLogParser.parseLineToPOJO(null), "Method should return null if argument is null"),
                () -> assertNull(fcrLogParser.parseLineToPOJO(""), "Method should return null if argument is empty string"),
                () -> assertNull(fcrLogParser.parseLineToPOJO(" "), "Method should return null if argument is blank string"),
                () -> assertNull(fcrLogParser.parseLineToPOJO("\n"), "Method should return null if argument is special character"),
                () -> assertNull(fcrLogParser.parseLineToPOJO("wyslijPlik:poczatek:1524473057732:DMXX-ERJ2-2FHK-XJ2D"), "Method should return null if sensor detected only start of process log"),
                () -> assertNull(fcrLogParser.parseLineToPOJO("wyslijPlik:koniec1524473057736:DMXX-ERJ2-2FHK-XJ2D"), "Method should return null if argument doesn't match pattern")
        );
    }

    @Test
    @DisplayName("Method should return correct metric")
    void shouldReturnMetric() {
        assertAll("MetricPOJO", () -> {
            fcrLogParser.parseLineToPOJO("wyslijPlik:poczatek:1524473057732:DMXX-ERJ2-2FHK-XJ2D");
            fcrLogParser.parseLineToPOJO("wyslijPlik:poczatek:1524473057732:DMXX-ERJ2-2FHK-IIII");
            MetricPOJO metricPOJO = fcrLogParser.parseLineToPOJO("wyslijPlik:koniec:1524473057736:DMXX-ERJ2-2FHK-XJ2D");
            assertNotNull(metricPOJO, "Method should return MetricPOJO object");

            assertAll(() -> assertEquals("metricName", metricPOJO.getMetricName(), "Metric name should be correct"),
                    () -> assertEquals(4.0, metricPOJO.getMetricValue(), "Metric value should be correct"),
                    () -> assertEquals(1524473057736L, metricPOJO.getTimestamp(), "Metric timestamp should be correct"));
        });
    }
}
package sensor.service.parser;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import sensor.common.pojo.MetricPOJO;

import static org.junit.jupiter.api.Assertions.*;

class CeTrafficLogParserTest {

    private static CeTrafficLogParser ceTrafficLogParser;

    @BeforeAll
    static void setup(){
        ceTrafficLogParser = new CeTrafficLogParser();
    }

    @Test
    @DisplayName("Method should return null if line is null, blank, etc.")
    void shouldReturnNullIfLineIsEmpty() {

        assertAll(() -> assertNull(ceTrafficLogParser.parseLineToPOJO(null), "Method should return null if argument is null"),
                () -> assertNull(ceTrafficLogParser.parseLineToPOJO(""), "Method should return null if argument is empty string"),
                () -> assertNull(ceTrafficLogParser.parseLineToPOJO(" "), "Method should return null if argument is blank string"),
                () -> assertNull(ceTrafficLogParser.parseLineToPOJO("\n"), "Method should return null if argument is special character")
        );
    }

    @Test
    @DisplayName("Method should return correct metric")
    void shouldReturnMetric() {

        assertAll("MetricPOJO", () -> {
            MetricPOJO metricPOJO = ceTrafficLogParser.parseLineToPOJO("ETPercentile 55.0 1522306062");
            assertNotNull(metricPOJO, "Method should return MetricPOJO object");

            assertAll(() -> assertEquals("ETPercentile", metricPOJO.getMetricName(),"Metric name should be correct"),
                    () -> assertEquals(55.0, metricPOJO.getMetricValue(),"Metric value should be correct"),
                    () -> assertEquals(1522306062L, metricPOJO.getTimestamp(),"Metric timestamp should be correct"));
        });
    }
}
package sensor.service.converter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import sensor.common.pojo.MetricPOJO;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PlaceholderMetricConverterTest {

    private static PlaceholderMetricConverter allPlaceholderMetricConverter;

    private static final String PATTERN = "${metric.name} - ${metric.value} - ${metric.timestamp}";

    @BeforeAll
    static void setup() {
        allPlaceholderMetricConverter = new PlaceholderMetricConverter(PATTERN);
    }

    @ParameterizedTest
    @CsvSource({"metricName, 1.0, 1522748967, metricName - 1.0 - 1522748967",
            ", 1.0, 1522748967, ${metric.name} - 1.0 - 1522748967"})
    void testWithCsvSource(String metricName, double metricValue, long metricTimestamp, String result) {
        MetricPOJO metricPOJO = new MetricPOJO(metricName, metricValue, metricTimestamp);
        assertEquals(result, allPlaceholderMetricConverter.convert(metricPOJO));
    }


}
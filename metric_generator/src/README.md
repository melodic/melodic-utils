# Metric-generator #

Sensor to sending metrics (e.g. response time) from file with logs or from json file by JMS or telnet connection

## Usage ##

### Building ###

-**mvn clean compile assembly:single** - creates jar with all needed dependencies (e.g. metric-generator-{version}-jar-with-dependencies.jar)


### Running ###

-**sudo java -jar metric-generator-{version}-jar-with-dependencies.jar -p {path to properties file}**

### Properties file example ###


        # ---------------- basic properties----------------
        #options: jms, telnet
        sender.type=telnet
        
        #options: file, url
        collector=file
        
        #for jms often please set ResponseTime, for telnet often: TR_AVG
        metric.name=ResponseTime
        
        #options: fcr, genom, ce_traffic
        collector.file.parser=fcr
        
        #time between checking (running cron) in secconds
        collector.checking.time=40
        
        #pattern for metric to create message. Avalible placeholders: metric.name, metric.value, metric.timestamp
        metric.pattern=${metric.name} - ${metric.value} - ${metric.timestamp}
        
        # --------------- transmitting by telnet -----------------
        
        telnet.server.address=localhost
        telnet.sever.port=12345
        
        # ---------------- transmitting by jms -------------
        
        #jms.server.address=localhost
        #jms.sever.port=8081
        
        # ------------ for creating metrics from logs files ---------------
        
        #direcory with file to analysing
        collector.file.dir=/var/log/tomcat7/
        
        #file pattern for log files
        collector.file.pattern=localhost_access_log.(\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])*).txt
        
        #file with information about state of analysis files with logs - creating by sensor
        collector.file.index=/home/ubuntu/metric-collector/file.idx

package generator;

import common.FileConverter;
import common.TrafficGeneratorProperties;
import cuckooService.wsdl.UsunPlikResp;
import cuckooService.wsdl.WyslijPlikResp;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class TrafficGenerator {

    private FcrClient soapClient;
    private FileConverter fileConverter;
    private TrafficGeneratorProperties properties;

    public void generateTraffic() throws InterruptedException {
        int numberOfRequestsPerSecond = properties.getNumberOfRequestsPerSecond();
        while (true) {
            try {
                byte[] contentToSend = fileConverter.convertFileToByteArray(properties.getPathToFileToSend());
                WyslijPlikResp wyslijPlikResp = soapClient.sendFileContent(contentToSend);
                if (wyslijPlikResp != null) {
                    String sentFileId = wyslijPlikResp.getIdPliku();
                    log.info("File is sent, ID of file in response: {}", sentFileId);

                    if (properties.isDeletingFileAfterSending()) {
                        log.info("Deleting file from service after sending");
                        UsunPlikResp usunPlikResp = soapClient.deleteFile(sentFileId);
                        log.info("Deleted file with ID: {} result = {}", sentFileId, usunPlikResp.isZrobione());
                    }
                }
                Thread.sleep((60 / numberOfRequestsPerSecond) * 1000);
            } catch (Exception e) {
                log.error("Error by sending request to server", e);
                Thread.sleep((60 / numberOfRequestsPerSecond) * 1000);
            }
        }
    }
}
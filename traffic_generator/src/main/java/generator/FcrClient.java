package generator;

import common.TrafficGeneratorProperties;
import cuckooService.wsdl.UsunPlikReq;
import cuckooService.wsdl.UsunPlikResp;
import cuckooService.wsdl.WyslijPlikReq;
import cuckooService.wsdl.WyslijPlikResp;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.SoapMessageCreationException;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

public class FcrClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(FcrClient.class);
    private TrafficGeneratorProperties properties = TrafficGeneratorProperties.getInstance();

    public FcrClient() throws Exception{
        configureMarshaller();
    }

    private void configureMarshaller() throws Exception{
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("cuckooService.wsdl");
        marshaller.afterPropertiesSet();

        setMarshaller(marshaller);
        setUnmarshaller(marshaller);
    }

    private WebServiceMessageCallback webServiceMessageCallback = message -> {
        try {
            SoapMessage soapMessage = (SoapMessage) message;
            SoapHeader header = soapMessage.getSoapHeader();
            StringSource userNameSource = new StringSource(
                    "<userName xmlns=\"http://7bulls.com/services/sic/xsd\">" + properties.getUsername() + "</userName>");
            StringSource passwordSource = new StringSource(
                    "<password xmlns=\"http://7bulls.com/services/sic/xsd\">" + properties.getPassword() + "</password>");

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(userNameSource, header.getResult());
            transformer.transform(passwordSource, header.getResult());
        } catch (Exception e) {
            log.error("Error by preparing request", e);
        }
    };

    public WyslijPlikResp sendFileContent(byte[] content){

        WyslijPlikReq request = new WyslijPlikReq();
        WyslijPlikResp response = null;
        request.setZawartosc(content);

        log.info("Request of sending file in bytes with array length: {}", content.length);

        try {
            response = (WyslijPlikResp) getWebServiceTemplate()
                    .marshalSendAndReceive(properties.getServiceUrl(), request, webServiceMessageCallback);
            log.info("Response ID of file: {}", response.getIdPliku());
        } catch (SoapMessageCreationException ex) {
            log.error("Error by connecting with soap server: ", ex);
        }

        return response;
    }

    public UsunPlikResp deleteFile(String fileId){

        log.info("Deleting file with id: {}", fileId);
        UsunPlikReq request = new UsunPlikReq();
        UsunPlikResp response = null;
        request.setIdPliku(fileId);

        try {
            response = (UsunPlikResp) getWebServiceTemplate()
                    .marshalSendAndReceive(properties.getServiceUrl(), request, webServiceMessageCallback);
            log.info("File with ID = {} is deleted = {}", fileId, response.isZrobione());
        } catch (SoapMessageCreationException ex) {
            log.error("Error by connecting with soap server: ", ex);
        }
        return response;
    }
}
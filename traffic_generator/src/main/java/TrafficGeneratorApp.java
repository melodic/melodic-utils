import common.Arguments;
import common.FileConverter;
import common.TrafficGeneratorProperties;
import generator.FcrClient;
import generator.TrafficGenerator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TrafficGeneratorApp {

    public static void main(String[] args) {
        try {
            Arguments arguments = Arguments.fromMain(args);
            TrafficGeneratorProperties.createInstance(arguments.getPathToPropertiesFile());
            TrafficGenerator trafficGenerator = new TrafficGenerator(new FcrClient(), new FileConverter(), TrafficGeneratorProperties.getInstance());
            trafficGenerator.generateTraffic();
        } catch (Exception e) {
            log.error("Error by generating traffic: ", e);
        }
    }
}
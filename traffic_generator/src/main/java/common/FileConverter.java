package common;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class FileConverter {

    private static final String DEFAULT_FILE_TO_SEND_NAME = "defaultFileToSend.txt";

    public byte[] convertFileToByteArray(String pathToFile) throws IOException {
        Path path = Paths.get(pathToFile);
        byte[] result;
        try {
            result = Files.readAllBytes(path);
        } catch (IOException ex) {
            log.info("File form path from properties file doesn't exist, getting default file from properties");
            result = convertFileFromResourcesToByteArray();
        }
        return result;
    }

    private byte[] convertFileFromResourcesToByteArray() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File fileToConvert = new File(classLoader.getResource(DEFAULT_FILE_TO_SEND_NAME).getFile());
        Path path = Paths.get(fileToConvert.getPath());
        return Files.readAllBytes(path);
    }

}

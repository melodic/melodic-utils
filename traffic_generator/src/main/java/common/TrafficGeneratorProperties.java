package common;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Slf4j
public class TrafficGeneratorProperties {

    private static TrafficGeneratorProperties INSTANCE = null;

    private static Properties prop;
    private boolean isInitialized;

    private TrafficGeneratorProperties(String pathToPropertiesFile) throws IOException {
        readProperties(pathToPropertiesFile);

        TrafficGeneratorProperties.TrafficGeneratorPropertiesValidator.ValidationResult validationResult = new TrafficGeneratorProperties.TrafficGeneratorPropertiesValidator().validate();
        if (!validationResult.isValid()) {
            validationResult.getErrors().forEach((enumName, information) -> log.error("Property '{}' - {}", enumName.getName(), information));
            throw new IllegalArgumentException("Exception by parsing properties");
        }

        INSTANCE = this;
    }

    public static TrafficGeneratorProperties getInstance() {
        if (INSTANCE == null || !INSTANCE.isInitialized)
            throw new RuntimeException("Properties for Traffic Generator is not set");
        return INSTANCE;
    }

    public static void createInstance(String pathToPropertiesFile) throws IOException {
        INSTANCE = new TrafficGeneratorProperties(pathToPropertiesFile);
        INSTANCE.isInitialized = true;
    }

    private void readProperties(String pathToPropertiesFile) throws IOException {
        log.info("Loading properties from: {}", pathToPropertiesFile);

        try {
            prop = new Properties();
            if (pathToPropertiesFile == null || pathToPropertiesFile.isEmpty()) {
                prop.load(TrafficGeneratorProperties.class.getResourceAsStream("/traffic.generator.properties"));
            } else {
                prop.load(new FileInputStream(pathToPropertiesFile));
            }
            this.isInitialized = true;
            log.info("Traffic Generator properties loaded");
        } catch (IOException ex) {
            log.error(ex.getMessage());
            throw ex;
        }
    }

    public String getServiceUrl() {
        return get(PropertyFieldName.SERVICE_URL);
    }

    public int getNumberOfRequestsPerSecond() {
        return Integer.valueOf(get(PropertyFieldName.NUMBER_OF_REQUESTS_PER_SECOND));
    }

    public String getPathToFileToSend() {
        return get(PropertyFieldName.PATH_TO_FILE_TO_SENDING);
    }

    public boolean isDeletingFileAfterSending() {
        return Boolean.valueOf(get(PropertyFieldName.DELETING_FILE_AFTER_SENDING));
    }

    public String getUsername() {
        return get(PropertyFieldName.USERNAME);
    }

    public String getPassword() {
        return get(PropertyFieldName.PASSWORD);
    }

    private String get(PropertyFieldName propertyFieldName) {
        return prop.getProperty(propertyFieldName.getName());
    }


    @Getter
    @AllArgsConstructor
    enum PropertyFieldName {

        SERVICE_URL("service.url"),

        USERNAME("username"),

        PASSWORD("password"),

        NUMBER_OF_REQUESTS_PER_SECOND("number.of.requests.per.second"),

        PATH_TO_FILE_TO_SENDING("path.to.file.to.sending"),

        DELETING_FILE_AFTER_SENDING("deleting.file.from.service.just.after.sending");


        private String name;

    }

    class TrafficGeneratorPropertiesValidator {

        ValidationResult validate() {
            Map<PropertyFieldName, String> errors = new HashMap<>();

            validateIfBlank(errors, PropertyFieldName.SERVICE_URL, getServiceUrl());
            validateNumberOfRequestsPerSecond(errors);
            validateDeletingFileFlag(errors);
            return new ValidationResult(errors);
        }

        private void validateDeletingFileFlag(Map<PropertyFieldName, String> errors) {
            String deletingFileFlag = get(PropertyFieldName.DELETING_FILE_AFTER_SENDING).trim();
            if(BooleanUtils.toBooleanObject(deletingFileFlag) == null){
                errors.put(PropertyFieldName.DELETING_FILE_AFTER_SENDING, String.format("Value: %s couldn't be converted to boolean", deletingFileFlag));
            }
        }

        private void validateNumberOfRequestsPerSecond(Map<PropertyFieldName, String> errors) {
            String numberOfRequestsPerSecond = get(PropertyFieldName.NUMBER_OF_REQUESTS_PER_SECOND);
            try {
                Integer.parseInt(numberOfRequestsPerSecond);
            } catch (NumberFormatException ex) {
                errors.put(PropertyFieldName.NUMBER_OF_REQUESTS_PER_SECOND, String.format("Value: %s is not a integer number", numberOfRequestsPerSecond));
            }
        }

        private void validateIfBlank(Map<PropertyFieldName, String> errors, PropertyFieldName propertyName, String value) {
            if (StringUtils.isBlank(value)) {
                errors.put(propertyName, "Value must be set");
            }
        }


        @Getter
        @AllArgsConstructor(access = AccessLevel.PRIVATE)
        class ValidationResult {
            private boolean valid;
            private Map<PropertyFieldName, String> errors;

            ValidationResult(Map<PropertyFieldName, String> errors) {
                this.valid = errors.isEmpty();
                this.errors = errors;
            }
        }
    }
}
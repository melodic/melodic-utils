Disclaimer -- INSECURE --

Please be aware that the implementation currently passes a VM's private key as url parameter and can therefore only be seen as a development 
tool. Do not use it in production! This feature is currently meant for evaluation and debugging purposes only!

The later use of SSL will improve transport security but NOT lower the risk of having the key in the browser's address bar.

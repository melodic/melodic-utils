#!/usr/bin/env bash

if [ "$USE_HTTPS" = true ] && [ ! -z "$CERT_FILE" ] && [ ! -z "$KEY_FILE" ] && [ ! -z "$HTTPS_PORT" ]
then
      echo "Launching WEB-SSH in SECURE mode."
      wssh --sslport=$HTTPS_PORT --address=0.0.0.0 --certfile=$CERT_FILE --keyfile=$KEY_FILE
elif [ ! -z "$PORT" ]
then
      echo "Launching WEB-SSH in NON-SECURE mode."
      wssh --port=$PORT --address=0.0.0.0
else
      echo "Mandatory variables not set. Exiting."
fi

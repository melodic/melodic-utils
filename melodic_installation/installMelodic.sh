#!/bin/bash

#created by mprusinski 2017.11.10

apt_update() {
    unset UCF_FORCE_CONFFOLD
    export UCF_FORCE_CONFFNEW=YES
    ucf --purge /boot/grub/menu.lst
    export DEBIAN_FRONTEND=noninteractive
    sudo -E apt-get update
    sudo -E apt-get -o Dpkg::Options::="--force-confold" --force-yes -fuy dist-upgrade
}

install_docker(){

  sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get update
  sudo apt-get install -y docker-ce docker-ce-cli containerd.io
  sudo cp ~/melodic-utils/melodic_properties/dev/docker/daemon.json /etc/docker/

}

install_melodic_without_docker(){

 #git clone https://bitbucket.7bulls.eu/scm/mel/utils.git
 #./utils/melodic_installation/installMelodic.sh install

#  apt_update

  echo "Changing IP in config files to public IP of current machine:" ${MELODIC_IP}
  sudo service docker restart
  cp ~/melodic-utils/melodic_properties/templates/melodicStack/docker-compose.yml ~/docker-compose.yml
  cp ~/melodic-utils/melodic_properties/templates/melodicStack/envtemplate ~/.env

  #install tools
  sudo apt-get install -y nmap grc
  cp -R ~/melodic-utils/melodic_installation/.grc ~/.grc
  cp -R ~/melodic-utils/melodic_installation/.tools ~/.tools
  cp ~/melodic-utils/melodic_installation/.profile ~/.profile
  sudo sed -i "s/MELODIC_IP/${MELODIC_IP}/g" ~/.tools/test_melodic_connections.sh

  #change directory permissions
  sudo sed -i "s/PUBLICIP/${MELODIC_IP}/g" ~/.env
  sudo sed -i "s/cloudiator.http.host=.*/cloudiator.http.host=${MELODIC_IP}/g" ~/conf/eu.melodic.integration.mule.properties

  sudo sed -i "s/host=.*/host=${MELODIC_IP}/g" ~/melodic-utils/cdo-uploader/src/main/resources/eu.paasage.mddb.cdo.client.properties

  echo "************************************** TOOLS **************************************"
  curl -sSf https://moncho.github.io/dry/dryup.sh | sudo sh
  sudo chmod 755 /usr/local/bin/dry
  sudo apt-get install -y xclip

  cd ~
  sudo docker-compose pull

  if [ -f ~/docker/docker-compose.yml ]; then
    cd ~/docker && sudo docker-compose pull
  fi

  sudo docker network create elk
  sudo sysctl -w vm.max_map_count=262144
}

install_melodic() {

  install_docker

  install_melodic_without_docker
}

install_cloudiator() {

  #git clone https://bitbucket.7bulls.eu/scm/mel/utils.git
  #./utils/melodic_installation/installCloudiatorV2.sh install

  install_docker

  COMPOSE_VERSION=1.25.1
  #COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9][0-9]+\.[0-9]+$" | tail -n 1`
  sudo sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
  sudo chmod +x /usr/local/bin/docker-compose


  #download cloudiator modules building on the fly
  cd ~
  git clone -b melodic-stable --recurse-submodules https://github.com/cloudiator/docker.git

  #configure cloudiator
  sudo cp ~/docker/env-template ~/docker/.env
  sudo sed -i "s/HOST_IP=.*/HOST_IP=${MELODIC_IP}/g" ~/docker/.env
  sudo sed -i "s/JMS_IP=.*/JMS_IP=${MELODIC_IP}/g" ~/docker/.env

  echo "" >> ~/docker/.env
  echo "#DISABLED_SERVICES=portainer,cadvisor,kibana,logstash,elasticsearch,kafka-manager,chronograf,influx,etcd-browser,yaml-agent,interface" >> ~/docker/.env
  echo "" >> ~/docker/.env

  sudo sed -i "s/# IMAGE_PREFIX=.*/IMAGE_PREFIX=88.99.85.63:5000\/stable-/g" ~/docker/.env

  #activate some properties disabled by default by Cloudiator
  sudo sed -i "s/# JMS_IP/JMS_IP/g" ~/docker/.env
  sudo sed -i "s/# JMS_PORT/JMS_PORT/g" ~/docker/.env
  sudo sed -i "s/# APP_NAME/APP_NAME/g" ~/docker/.env
  sudo sed -i "s/# METRIC_PATTERN/METRIC_PATTERN/g" ~/docker/.env
  sudo sed -i "s/# METRIC_REPORTING_INTERVAL/METRIC_REPORTING_INTERVAL/g" ~/docker/.env
  sudo sed -i "s/# DEPLOYMENT_INSTALL_MELODIC_TOOLS=.*/DEPLOYMENT_INSTALL_MELODIC_TOOLS=true/g" ~/docker/.env
  sudo sed -i "s/# INSTALLER_VISOR_DOWNLOAD=.*/INSTALLER_VISOR_DOWNLOAD=https:\/\/s3-eu-west-1.amazonaws.com\/melodic.testing.data\/ARTIFACTS\/visor\/visor-service-0.3.0-SNAPSHOT.jar/g" ~/docker/.env
  sudo sed -i "s/# INSTALLER_LANCE_DOWNLOAD=.*/INSTALLER_LANCE_DOWNLOAD=https:\/\/s3-eu-west-1.amazonaws.com\/melodic.testing.data\/ARTIFACTS\/lance\/server-0.3.0-SNAPSHOT.jar/g" ~/docker/.env
  sudo sed -i "s/# DLMS_WEBSERVICE_PORT/DLMS_WEBSERVICE_PORT/g" ~/docker/.env
  sudo sed -i "s/# INSTALLER_DLMSAGENT_DOWNLOAD=.*/INSTALLER_DLMSAGENT_DOWNLOAD=https:\/\/s3-eu-west-1.amazonaws.com\/melodic.testing.data\/ARTIFACTS\/ow2\/dlmsagent\/DLMSAgent-3.1.0-SNAPSHOT.jar/g" ~/docker/.env
  sudo sed -i "s/# INFLUX_URL=.*/INFLUX_URL=http:\/\/localhost:8086/g" ~/docker/.env
  sudo sed -i "s/# INFLUX_PASSWORD.*/INFLUX_PASSWORD=dummy/g" ~/docker/.env


  #re-configure docker-compose.yml file of Cloudiator.
  #this is tricky as we insert at specific line numbers, but there is no good way to do it. We want to avoid keeping/maintaining second docker-compose.yml file that would need to be synced with original one.
  cp ~/melodic-utils/melodic_properties/templates/melodicStack/executionware/docker-compose.yml ~/docker/docker-compose.yml

  cp ~/melodic-utils/melodic_properties/templates/melodicStack/executionware/docker-compose.yml ~/docker/docker-compose.yml

  sudo mkdir ~/docker/store
  sudo mkdir ~/docker/encryption-agent

  #add hostname to /etc/hosts
  sudo rm /etc/hosts
  echo 127.0.0.1 localhost.localdomain localhost `hostname` | sudo tee /etc/hosts

  #copy certs
  sudo cp ~/conf/common/melodic-truststore.p12 ~/docker/install-agent/truststore.p12
  sudo cp ~/conf/common/melodic-truststore.p12 ~/docker/install-agent/keystore.p12

  USER=`who am i | awk '{print $1}'`
  sudo chown -R $USER:$USER ~/docker

}

prepare_common_folders(){
  #create default folders
  cd ~/
  mkdir -p ~/conf/common
  mkdir -p ~/logs
  mkdir -p ~/certs
  mkdir -p ~/logs/log

  USER=`who am i | awk '{print $1}'`
  cp -R ~/melodic-utils/melodic_properties/templates/config/* ~/conf
  sudo chown -R $USER:$USER ~/conf
  sudo chown -R $USER:$USER ~/logs
  sudo chown -R $USER:$USER ~/certs
  sudo chown -R $USER:$USER ~/logs/log

}
install_melodic_with_cloudiator() {

  get_public_ip
  prepare_common_folders
  apt_update
  generate_certs
  install_cloudiator
  install_melodic_without_docker
}

generate_certs() {

cd ~/

if [[ -z $MELODIC_CONFIG_DIR ]]; then MELODIC_CONFIG_DIR=${BASEDIR}/conf; export MELODIC_CONFIG_DIR; fi

TRUSTSTORE_FILE=~/conf/common/melodic-truststore.p12

# Creation of key pairs, certificates of all components and population of common truststore
MELODIC_SERVICES=(cdoserver mule adapter generator cpsolver camunda memcache ldap metasolver jwtserver authdb authserver dlmswebservice dlmscontroller ems gui-backend functionizer-testing-tool gui-frontend cloudiator ui-webssh ptsolver ncsolver geneticsolver mctssolver)

for i in ${MELODIC_SERVICES[*]}; do
    create_keystore_for $i
done

extract_key_certs_for_gui

echo Key stores, certificate and Melodic common truststores are ready.

}

regenerate_certs() {
echo "Regenerating certificates for IP: " $1
MELODIC_IP=$1
rm -rf ~/conf/common/melodic-truststore.p12
generate_certs
#copy certs
cp ~/conf/common/melodic-truststore.p12 ~/docker/install-agent/truststore.p12
cp ~/conf/common/melodic-truststore.p12 ~/docker/install-agent/keystore.p12

}

# Definition of 'create_keystore_for' function for the:
# Creation of key pair and certificate for component
function create_keystore_for() {

    # Keystore initialization settings
    KEY_GEN_ALG=RSA
    KEY_SIZE=2048
    START_DATE=-1d
    VALIDITY=3650
    DN_FMT="CN=%s,OU=Information Management Unit (IMU),O=Institute of Communication and Computer Systems (ICCS),L=Athens,ST=Attika,C=GR"
    EXT_SAN_FMT="SAN=dns:%s,dns:localhost,ip:127.0.0.1,ip:${MELODIC_IP}"
    KEYSTORE_TYPE=PKCS12
    KEYSTORE_PASS=melodic

    local COMPONENT=$1
    local KEYSTORE_DIR=~/certs/${COMPONENT}
    local KEYSTORE_FILE=${KEYSTORE_DIR}/keystore.p12
    local CERT_FILE=${KEYSTORE_DIR}/${COMPONENT}.crt
    local KEY_ALIAS=${COMPONENT}
    local DN=`printf "${DN_FMT}" "${KEY_ALIAS}" `
    local EXT_SAN=`printf "${EXT_SAN_FMT}" "${KEY_ALIAS}" `

    echo "$COMPONENT:"
    mkdir -p ${KEYSTORE_DIR}

    #install java
    sudo apt-get install -y openjdk-8-jre-headless

    echo "  Generating key pair and certificate for ${COMPONENT}..."
    rm -f ${KEYSTORE_FILE} &> /dev/null
    keytool -genkey -keyalg ${KEY_GEN_ALG} -keysize ${KEY_SIZE} \
            -alias ${KEY_ALIAS} \
            -startdate ${START_DATE} -validity ${VALIDITY} \
            -dname "${DN}" -ext "${EXT_SAN}" \
            -keystore ${KEYSTORE_FILE} \
            -storetype ${KEYSTORE_TYPE} -storepass ${KEYSTORE_PASS}

    echo "  Exporting certificate of ${COMPONENT}..."
    rm -rf ${CERT_FILE} &> /dev/null
    keytool -export \
            -alias ${KEY_ALIAS} \
            -file ${CERT_FILE} \
            -keystore ${KEYSTORE_FILE} \
            -storetype ${KEYSTORE_TYPE} -storepass ${KEYSTORE_PASS}

    echo "  Importing ${COMPONENT} certificate to truststore..."
    keytool -import -noprompt \
            -alias ${KEY_ALIAS} \
            -file ${CERT_FILE} \
            -keystore ${TRUSTSTORE_FILE} \
            -storetype ${KEYSTORE_TYPE} -storepass ${KEYSTORE_PASS}

    if [ "${COMPONENT}" = "ui-webssh" ]
    then
        echo "genereting openssl certs..."
        openssl pkcs12 -in ${KEYSTORE_FILE} -nodes -nokeys -password pass:${KEYSTORE_PASS} -out ${KEYSTORE_DIR}/${COMPONENT}.cert
        openssl pkcs12 -in ${KEYSTORE_FILE} -nodes -nocerts -password pass:${KEYSTORE_PASS} -out ${KEYSTORE_DIR}/${COMPONENT}.key
    fi


    echo ""
}

extract_key_certs_for_gui() {
    cd ~/certs/gui-frontend
    sudo sh -c 'openssl pkcs12 -in keystore.p12 -nocerts -nodes -password pass:melodic |
        sed -ne "/-BEGIN PRIVATE KEY-/,/-END PRIVATE KEY-/p" > gui-frontend.key'
    sudo sh -c 'openssl pkcs12 -in keystore.p12 -clcerts -nokeys -password pass:melodic |
        sed -ne "/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p" > gui-frontend-server.crt'

    cd ~/certs/gui-backend
    sudo sh -c 'openssl pkcs12 -in keystore.p12 -clcerts -nokeys -password pass:melodic |
        sed -ne "/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p" > gui-backend-client.crt'

    echo "Key and certificates for Melodic GUI successfully extracted"
}

get_public_ip() {

read -d '' urls << 'END'
http://ifconfig.me/
http://icanhazip.com/
http://ident.me/
http://whatismyip.akamai.com/
http://bot.whatismyipaddress.com/
http://l2.io/ip
http://eth0.me/
http://ipecho.net/plain
http://ifconfig.me
http://bot.whatismyipaddress.com
http://checkip.amazonaws.com
END

    ipslist=()

    for url in $urls
    do
        echo -e "\n$url"
        cout=$(curl -s -m10 -L $url | tr -d '\n')
        answer=$(echo $cout | cut -d'~' -f1)
        echo answer: $answer
        ipslist+=($answer)
    done

    #make unique
    ips=($(echo "${ipslist[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

    for ip in "${ips[@]}"
    do
        echo element: $ip
    done

    clear

    echo ""
    echo "Please indicate which PUBLIC IP would you like to use?"
    echo "-----------------------------------------------------------"
    echo ""

    i=0;
    for ip in "${ips[@]}"
    do
      printf "%s\t%s\n" "[$((i+1))] -->" "${ips[$i]}"
      i=$((i+1))
    done

    if [ -z "$1" ] ; then read -p "Selected IP: " selectedIndex; else selectedIndex=$1 ; fi

    selectedIndex=$((selectedIndex-1))

    export MELODIC_IP=${ips[$selectedIndex]}
    echo "Using IP: " $MELODIC_IP

}



## main logic ###
case "$1" in

  regenerate_certs)
        regenerate_certs $2
        ;;
#  install_melodic)
#        install_melodic
#        ;;
#  install_melodic_with_cloudiator)
#        install_melodic_with_cloudiator
#         ;;
    *)
        install_melodic_with_cloudiator
esac

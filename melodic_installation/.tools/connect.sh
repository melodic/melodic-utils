#!/bin/bash
#if [ -z "$1" ] || [ -z "$2" ]; then echo "Usage: vmconnect [qb1|qb3|cas] [IP]"; return 1;  fi

# --------------------BEGIN_CONFIG--------------------
instanceUser=ubuntu
# ---------------------END_CONFIG---------------------

readarray a < connect.config

clear

echo ""
echo "Please indicate what Melodic environment would you like to use:"
echo "-----------------------------------------------------------"
echo ""

for i in "${!a[@]}"; do
  printf "%s\t%s\n" "[$((i+1))] -->" "${a[$i]}"
done

if [ -z "$1" ] ; then read -p "Selected instance: " selectedIndex; else selectedIndex=$1 ; fi



selectedIndex=$((selectedIndex-1))
name="$(cut -d';' -f1 <<<${a[${selectedIndex}]})"
connectionString="$(cut -d';' -f2 <<<${a[${selectedIndex}]})"
key="$(cut -d';' -f3 <<<${a[${selectedIndex}]})"

echo "Fetching SSH private key and IPs from:" $name

#select ip.ip, vm.generatedPrivateKey, vm.name from VirtualMachine vm join IpAddress ip on vm.id = ip.virtualMachine_id;
echo "Connecting to: " $connectionString
ssh -i $key $connectionString 'mysql -uroot -pmelodic -t -e "use colosseum; select ip.ip, vm.name from VirtualMachine vm join IpAddress ip on vm.id = ip.virtualMachine_id where ip.ipType=\"PUBLIC\" ;"' > ips.txt

ssh -i $key $connectionString 'mysql -uroot -pmelodic -t -e "use colosseum; select vm.generatedPrivateKey from VirtualMachine vm LIMIT 1;" '  > key.txt
cat key.txt | sed -n ''/BEGIN/,/END/p'' | sed '$ s/.\{2\}$//' | sed '1 s/^..//' > key.txt

cat ips.txt | sed -e '1,3d' > ips.txt


#ubuntu@ip-172-31-39-89:~$ mysql -uroot -pmelodic -e "use colosseum; select CONCAT(ip.ip,"ASD ", vm.generatedPrivateKey,"ASD ", vm.name) from VirtualMachine vm join IpAddress ip on vm.id = ip.virtualMachine_id where ip.ipType="PUBLIC" ;" > temp


#ssh -i $key $connectionString ./queryIPAddresses.sh | tail -n +4 | head -n -1 > ips.txt

#ssh -i $key $connectionString ./queryVMTable.sh  | sed -n ''/BEGIN/,/END/p'' | sed '$ s/.\{2\}$//' | sed '1 s/^..//' > key.txt

echo "downloaded SSH key..."

readarray b < ips.txt

clear


echo ""
echo "Please indicate what instance you would like to connect to:"
echo "-----------------------------------------------------------"
echo ""

for i in "${!b[@]}"; do
  printf "%s\t%s\n" "[$((i+1))] -->" "${b[$i]}"
done


if [ -z "$2" ] ; then read -p "Selected instance: " selectedVMIndex; else selectedVMIndex=$2 ; fi

selectedVMIndex=$((selectedVMIndex-1))

ip="$(cut -d'|' -f2 <<<${b[${selectedVMIndex}]})"

echo "Starting to connect to IP" $ip
cleanIP="$(echo -e "${ip}" | tr -d '[:space:]')"

echo ""
 ssh -oStrictHostKeyChecking=no -i key.txt $instanceUser@$cleanIP

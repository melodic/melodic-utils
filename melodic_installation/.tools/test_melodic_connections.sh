#!/bin/bash

parse_file(){

  # IP of host to check
  HOST_IP=localhost

  file=$1
  PREVIOUS_LINE=""
  SEARCHING_LINE_FRAGMENT_IMAGE="image:"
  SEARCHING_LINE_FRAGMENT_DEPENDS="depends_on:"
  SEARCHING_LINE_FRAGMENT_BUILD="build:"
  SEARCHING_LINE_FRAGMENT_PORTS="ports:"
  EXISTS_DEPENDS_ON_STATEMENT=false
  EXISTS_BUILD_STATEMENT=false

  PORT_REGEX="^\-.*[0-9]{2,5}:[0-9]{2,5}\"{0,1}$"
  PORT_PREFIX="- "
  PORT_PREFIX_FOR_CLOUDIATOR='"'
  PORT_NUMBER=""

  SERVICE_NAME=""
  SERVICE_NAME_SUFIX=":"

  MAIN_ARRAY=()

  #turn trimming line on
  shopt -s extglob


  while IFS= read -r line
  do

    #trim line for parsing ports
    line="${line##*( )}"

    #stop processing line with comments
    if [[ $line = \#* ]] ; then
      continue
    fi

    #parsing services names
    if [[ $line == *"$SEARCHING_LINE_FRAGMENT_IMAGE"* ]] && [[ "$EXISTS_DEPENDS_ON_STATEMENT" == false ]]; then
      SERVICE_NAME="$PREVIOUS_LINE"

      #remove service name suffix from service name
      SERVICE_NAME=${SERVICE_NAME%"$SERVICE_NAME_SUFIX"}
    fi

    if  [[ $line == *"$SEARCHING_LINE_FRAGMENT_DEPENDS"* ]] && [[ "$EXISTS_BUILD_STATEMENT" == false ]] && [[ ! "$PREVIOUS_LINE" == *"$SEARCHING_LINE_FRAGMENT_IMAGE"* ]]; then
      SERVICE_NAME="$PREVIOUS_LINE"

      #remove service name suffix from service name
      SERVICE_NAME=${SERVICE_NAME%"$SERVICE_NAME_SUFIX"}
      EXISTS_DEPENDS_ON_STATEMENT=true
    fi

    if  [[ $line == *"$SEARCHING_LINE_FRAGMENT_BUILD"* ]]; then
      SERVICE_NAME="$PREVIOUS_LINE"

      #remove service name suffix from service name
      SERVICE_NAME=${SERVICE_NAME%"$SERVICE_NAME_SUFIX"}
      EXISTS_BUILD_STATEMENT=true
    fi

    if [[ $line == *"$SEARCHING_LINE_FRAGMENT_IMAGE"* ]] && [[ "$EXISTS_DEPENDS_ON_STATEMENT" == true ]]; then
      EXISTS_DEPENDS_ON_STATEMENT=false
    fi

    if  [[ $line == *"$SEARCHING_LINE_FRAGMENT_DEPENDS"* ]] && [[ "$EXISTS_BUILD_STATEMENT" == true ]]; then
      EXISTS_DEPENDS_ON_STATEMENT=false
      EXISTS_BUILD_STATEMENT=false
    fi



    #parsing ports
    if [[ $line =~ $PORT_REGEX ]]; then
      IFS=':' read -ra ADDR <<< "$line"
      PORT_NUMBER=${ADDR[0]}

      #remove port prefix from port number
      PORT_NUMBER=${PORT_NUMBER#$PORT_PREFIX}
      PORT_NUMBER=${PORT_NUMBER#$PORT_PREFIX_FOR_CLOUDIATOR}

      #create sub array of name and port
      SUB_ARRAY=("$SERVICE_NAME" "$PORT_NUMBER")

      #add sub array to main array
      MAIN_ARRAY+=(${SUB_ARRAY[@]})
    fi

    PREVIOUS_LINE=$line

  done <"$file"


  # turn trimming line off
  shopt -u extglob

  #set colors for printf state of services
  RED='\033[0;31m'
  NC='\033[0m' # No Color
  GREEN='\033[0;32m'


  # Loop and print it.  Using offset and length to extract values
  COUNT=${#MAIN_ARRAY[@]}
  PREVIOUS_MESSAGE=""
  PREVIOUS_NAME=""

  for ((i=0; i<$COUNT; i+=2))
  do
    NAME=${MAIN_ARRAY[i]:0}

    PORT=${MAIN_ARRAY[i+1]:0}

    RET=`nmap -p ${PORT} -PS ${HOST_IP} | grep '/tcp *open ' | wc -l`
    RET_TEXT=""

    case "$RET" in
      "1") RET_TEXT="${GREEN} OK ${NC}" ;;
      *) RET_TEXT="${RED} NOK ${NC}"
    esac

    #concat message for the same service
    if [ "$NAME" == "$PREVIOUS_NAME" ]; then
      MESSAGE="${PORT}: ${RET_TEXT}"
      MESSAGE="$PREVIOUS_MESSAGE $MESSAGE"

    else
      MESSAGE="${NAME}: ${PORT}: ${RET_TEXT}"
      echo -e $PREVIOUS_MESSAGE
    fi

    PREVIOUS_MESSAGE=$MESSAGE
    PREVIOUS_NAME=$NAME
  done

  echo -e $PREVIOUS_MESSAGE
}

MELODIC_YML_FILE_PATH=~/docker-compose.yml
CLOUDIATOR_COMPOSE_FILE_PATH=~/docker/docker-compose.yml
# CLOUDIATOR_COMPOSE_FILE_PATH="disabled"

if [ -f $MELODIC_YML_FILE_PATH ]; then
  parse_file $MELODIC_YML_FILE_PATH
fi

if [ -f $CLOUDIATOR_COMPOSE_FILE_PATH ]; then
  printf "\n\nCLOUDIATOR SERVICES\n"
  parse_file $CLOUDIATOR_COMPOSE_FILE_PATH
fi

#!/bin/bash
CDO_USER="$1"
CDO_PASSWORD="$2"
CDO_REPOSITORY="$3"
if [ $# -ne 3 ]
then
        echo "Usage: $0 {MySQL-User-Name} {MySQL-User-Password} {CDO Server repository DB}"
        exit 1
fi
TABLES=$(mysql --protocol=TCP -u $CDO_USER -p$CDO_PASSWORD $CDO_REPOSITORY -e 'show tables' | awk '{ print $1}' | grep -v '^Tables' )
for t in $TABLES
do
        echo "CDO Reset: Deleting $t table from $CDO_REPOSITORY database..."
        mysql --protocol=TCP -u $CDO_USER -p$CDO_PASSWORD $CDO_REPOSITORY -e "SET FOREIGN_KEY_CHECKS = 0; drop table $t; SET FOREIGN_KEY_CHECKS = 1;"
done
from base64 import b64encode
from getpass import getpass
from hashlib import sha1
from pathlib import Path
from sys import version_info


"""
Creates ldif file for import to LDAP server (ldap php admin).
Password is hashed with SHA1.

Written in pure python 3.
"""


def get_creds():
    """
    Returns dict with username and properly hashed password the same way as LDAP does it.
    """

    user = str(input("User (default: user1): ") or "user1")
    secret = getpass(prompt="Password: ")
    try:
        assert secret == getpass(prompt="Password again: ")
    except AssertionError:
        print("Passwords differ, try again.")
        exit()

    secret = str.encode(secret)  # string -> bytes
    secret = sha1(secret).digest()  # SHA1 hash
    secret = b64encode(secret)  # bytes representation of base64'ed secret
    secret = secret.decode("utf-8")  # bytes -> string
    return {"user": user, "secret": secret}


def ldif_text(user, secret):
    """
    Mimics structure of 'ldif' file which is consumed by LDAP
    """

    raw = "{SHA}"  # cant put curly braces in fstring, duh
    txt = [
        "dn: cn={user},ou=ADMIN,dc=example,dc=org".format(user=user),
        "cn: {user}".format(user=user),
        "objectclass: person",
        "objectclass: top",
        "sn: {user}".format(user=user),
        "userpassword: {raw}{secret}".format(raw=raw, secret=secret),
        "pwdPolicySubentry: cn=ppolicy,dc=example,dc=org",
    ]
    return "\n".join(txt)


def save_ldif(fname, txt):
    with open(str(fname), "w") as f:  # pathlib doesnt like open() prior py3.6
        f.writelines(txt)


def prepare_dir(dirname):
    p = Path(dirname)
    p.mkdir(exist_ok=True)
    return p


if __name__ == "__main__":

    ver = version_info[0]
    assert ver == 3, "Run with Python 3, not {}.".format(ver)  # python 3 only

    creds = get_creds()
    user = creds["user"]
    secret = creds["secret"]
    outfolder = prepare_dir("tmp")  # folder name shall stay untouched, shell script relies on it
    outfile = "user.ldif"  # outfile shall stay untouched, shell script relies on it
    txt = ldif_text(user, secret)

    save_ldif(outfolder / outfile, txt)

    print("\n{outfile} created sucessfully.".format(outfile=outfile))

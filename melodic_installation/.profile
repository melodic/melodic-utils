# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"


alias tail='grc tail'
alias cat='grc cat'

get_public_ip() {

    read -d '' urls << 'END'
http://ifconfig.me/
http://icanhazip.com/
http://ident.me/
http://whatismyip.akamai.com/
http://bot.whatismyipaddress.com/
http://l2.io/ip
http://eth0.me/
http://icanhazip.com
http://ipecho.net/plain
http://ifconfig.me
http://bot.whatismyipaddress.com
http://checkip.amazonaws.com
END

    ipslist=()

    for url in $urls
    do
        echo -e "\n$url"
        cout=$(curl -s -m10 -L $url | tr -d '\n')
        answer=$(echo $cout | cut -d'~' -f1)
        echo answer: $answer
        ipslist+=($answer)
    done

    #make unique
    ips=($(echo "${ipslist[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

    for ip in "${ips[@]}"
    do
        echo element: $ip
    done

    clear

    echo ""
    echo "Please indicate which PUBLIC IP would you like to use?"
    echo "-----------------------------------------------------------"
    echo ""

    i=0;
    for ip in "${ips[@]}"
    do
      printf "%s\t%s\n" "[$((i+1))] -->" "${ips[$i]}"
      i=$((i+1))
    done

    if [ -z "$1" ] ; then read -p "Selected IP: " selectedIndex; else selectedIndex=$1 ; fi

    selectedIndex=$((selectedIndex-1))

    export MELODIC_IP=${ips[$selectedIndex]}
    echo "Using IP: " $MELODIC_IP

}

alias ipupdate='function _ipupdate(){
        get_public_ip
 	    #updates cloudiator and melodic IPs in env files to current public IP of the machine
        echo replacing IP with: $MELODIC_IP in ~/.env and ~/docker/.env files...
        sudo sed -i "s/MELODIC_IP=.*/MELODIC_IP=${MELODIC_IP}/g" ~/.env
        sudo sed -i "s/CLOUDIATOR_IP=.*/CLOUDIATOR_IP=${MELODIC_IP}/g" ~/.env
        sudo sed -i "s/HOST_IP=.*/HOST_IP=${MELODIC_IP}/g" ~/docker/.env
        sudo sed -i "s/JMS_IP=.*/JMS_IP=${MELODIC_IP}/g" ~/docker/.env
        sudo sed -i "s/cloudiator.http.host=.*/cloudiator.http.host=${MELODIC_IP}/g" ~/conf/eu.melodic.integration.mule.properties

        #regenerate certs
        sudo ~/melodic-utils/melodic_installation/installMelodic.sh regenerate_certs ${MELODIC_IP}
        sudo sysctl -w vm.max_map_count=262144

 	    };_ipupdate'

alias dundeploy='function _dundeploy(){
            cd ~
            sudo docker-compose down
            if [ -f ~/docker/docker-compose.yml ]; then
                cd ~/docker && sudo docker-compose down
            fi
                };_dundeploy'

alias doremi='sudo docker rmi $(sudo docker images -q) -f'
alias dps='sudo docker ps'
alias dpss='sudo docker ps -s'
alias dmi='sudo docker images'
alias dless='function _dless(){
            cat $1 | less -R ; };_dless'

alias dexec='function _blah(){
            sudo docker exec -it $1 /bin/bash; };_blah'
alias dbash='function _dbash(){

            containerID=$(sudo docker ps --format "'{{.ID}}:::{{.Names}}'"| grep $1 | head -c 12 )
            sudo docker exec -it $containerID /bin/bash
            };_dbash'

alias dlog='function _dlog(){

            containerID=$(sudo docker ps --format "'{{.ID}}:::{{.Names}}'"| grep $1 | head -c 12 )
            echo container id: $containerID
            case $1 in
		"mule") sudo docker exec -it $containerID tail -f /opt/mule-standalone-3.8.0/logs/mule-integration.log ;;
		*) sudo docker logs $containerID -f ;;
	    esac
            };_dlog'

alias djournal='function _djournal(){

            sudo journalctl CONTAINER_TAG=$1
            };_djournal'

alias drestart='function _drestart(){

            if [ -f ~/docker/docker-compose.yml ]; then
                cd ~/docker
                sudo docker-compose down
                cd ~/
            fi

            cd ~/
            sudo docker-compose down

            sudo service docker stop

            sleep 1
            cd ~
            sudo service docker start

            cd ~/
            source .env
            disabled=$(echo $DISABLED_SERVICES | tr "," "\n")
            options=""
            for service in $disabled
            do
                options=$options" --scale "$service"=0"
            done

            # sudo sh -c "docker-compose create >> ~/logs/log/upperware.log 2>&1 && sudo docker-compose up $options >> ~/logs/log/upperware.log 2>&1 &"
	        sudo sh -c "docker-compose up -d $options >> ~/logs/log/upperware.log 2>&1"

            if [ -f ~/docker/docker-compose.yml ]; then
                cd ~/docker
                source .env
                disabled=$(echo $DISABLED_SERVICES | tr "," "\n")
                options=""
                for service in $disabled
                do
                    options=$options" --scale "$service"=0"
                done
                # sudo sh -c "docker-compose create >> ~/logs/log/executionware.log 2>&1 && sudo docker-compose up $options >> ~/logs/log/executionware.log 2>&1 &"
                # sudo sh -c "docker-compose up -d $options >> ~/logs/log/executionware.log 2>&1"
		        sudo sh -c "docker-compose up -d >> ~/logs/log/executionware.log 2>&1"
		cd ~/
            fi
                };_drestart'

alias pull='sudo docker-compose pull'

alias mping='~/.tools/test_melodic_connections.sh'

alias drmc='sudo docker rm $(sudo docker ps -aq -f status=exited)'
alias drmi='sudo docker image prune -a'
alias drmi='sudo docker image prune -a'
alias mprune='function _mprune(){

            sudo docker image prune -a -f
            sudo docker system prune -f
            sudo docker volume rm -f $(sudo docker volume ls -q --filter dangling=true | grep -v melodic)
            sudo rm -Rf ~/logs/
            };_mprune'

#source env file
. .env

alias dstats='sudo docker ps -q | xargs sudo docker stats --no-stream'




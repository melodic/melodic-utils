#!/bin/bash
chmod +x tmp/ppolicymodule.ldif && sudo mv tmp/ppolicymodule.ldif /tmp/ppolicymodule.ldif
chmod +x tmp/ppolicyoverlay.ldif && sudo mv tmp/ppolicyoverlay.ldif /tmp/ppolicyoverlay.ldif
chmod +x tmp/ppolicy-default.ldif && sudo mv tmp/ppolicy-default.ldif /tmp/ppolicy-default.ldif
chmod +x tmp/group.ldif && sudo mv tmp/group.ldif /tmp/group.ldif
chmod +x tmp/technical_user.ldif && sudo mv tmp/technical_user.ldif /tmp/technical_user.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/ppolicymodule.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapadd -Y EXTERNAL -H ldapi:/// -a -f /tmp/ppolicyoverlay.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapmodify -x -a -H ldapi:/// -D cn=admin,dc=example,dc=org -w melodic -f /tmp/ppolicy-default.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapadd -h "$MELODIC_IP" -c -D cn=admin,dc=example,dc=org -w melodic -f /tmp/group.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapadd -h "$MELODIC_IP" -c -D cn=admin,dc=example,dc=org -w melodic -f /tmp/technical_user.ldif
cd ~/melodic-utils/melodic_installation
./addLdapUser.sh

#!/bin/bash

#created by mprusinski 2017.11.10
TEST_DIR_NAME=test
TEST_DIR=${HOME}/${TEST_DIR_NAME}

init (){
    cd ~
    sudo rm -rf ${TEST_DIR_NAME}
    sudo mkdir ${TEST_DIR_NAME}
    cd ${TEST_DIR_NAME}
    echo 'downloading test data'
    sudo git clone https://bitbucket.7bulls.eu/scm/tst/melodic.git
    echo 'downloading test data - finished'
    echo 'downloading uploader'
    sudo wget https://s3-eu-west-1.amazonaws.com/melodic.testing.data/cdo-uploader-1.0.1-SNAPSHOT-jar-with-dependencies.jar
    echo 'downloading uploader - finished'
    echo 'installing java'
    sudo apt-get install default-jdk -y
    echo 'installing java - finished'
    MYIP=`curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'`
    sudo cp  ~/utils/melodic_properties/templates/cdouploader/* ${TEST_DIR}
    sudo sed -i "s/MELODIC_IP/${MYIP}/g" ~/test/eu.paasage.mddb.cdo.client.properties
}

storeTwoComponentApp () {
    java -Deu.paasage.configdir=${TEST_DIR} -jar ${TEST_DIR}/cdo-uploader-1.0.1-SNAPSHOT-jar-with-dependencies.jar -mode single -path ${TEST_DIR}/melodic/TestCases/TwoComponentApp
    echo 'storing TwoComponentApp - finished'
}

storeOneComponentApp () {
    java -Deu.paasage.configdir=${TEST_DIR} -jar ${TEST_DIR}/cdo-uploader-1.0.1-SNAPSHOT-jar-with-dependencies.jar -mode single -path ${TEST_DIR}/melodic/TestCases/OneComponentApp
    echo 'storing OneComponentApp - finished'
}

### main logic ###
case "$1" in
  init)
        init
        ;;
  storeTwoComponentApp)
        storeTwoComponentApp
        ;;
  storeOneComponentApp)
        storeOneComponentApp
        ;;
    *)
        echo $"Usage: $0 {init, storeOneComponentApp, storeTwoComponentApp}"
        exit 1
esac
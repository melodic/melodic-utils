#!/usr/bin/env bash
python3 lib/create_ldif.py
chmod +x tmp/user.ldif && sudo mv tmp/user.ldif /tmp/user.ldif
sudo docker exec `sudo docker ps | grep "ldap" | awk '{print $1;}'` ldapadd -h "$MELODIC_IP" -c -D cn=admin,dc=example,dc=org -w melodic -f /tmp/user.ldif
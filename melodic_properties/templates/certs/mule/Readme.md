Mule certificates to handle HTTPS description:
By default mule keystore is located in ~/certs/mule
The mule public certificate (mule-server.crt) is stored in ~/conf/ and is used by the mule clients

How the files were generated:
1. Mule keystore (mule-keystore.p12):
keytool -genkeypair -keystore mule-keystore.p12 -storetype PKCS12 -dname "CN=mule" -keypass password -storepass password -keyalg RSA -sigalg SHA1withRSA  -keysize 2048 -alias mule -ext SAN=DNS:mule,IP:127.0.0.1 -validity 3650

2. Mule server crt file (mule-server.crt):
keytool -export -storetype PKCS12 -keystore mule-keystore.p12 -storepass password -alias mule -file mule-server.crt

The mule-server.crt file is then being injected to the each client's truststore during creation of docker image with the following command:
keytool -noprompt -storetype PKCS12 -storepass changeit -import -alias mule -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts -file /config/mule-server.crt
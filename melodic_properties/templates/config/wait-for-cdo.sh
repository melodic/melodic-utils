#!/bin/bash

set -e

cmd="$@"

until nc -z $CDO_HOST $CDO_PORT; do
    echo "$(date) - waiting for cdo..."
    sleep 1
done

exec $cmd


# See http://docs.opscode.com/config_rb_knife.html for more information on knife configuration options
#
current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "ecatcetic"
client_key               "#{ENV['HOME']}/.ssh/ecatcetic.pem"
validation_client_name   "paasage-cetic-validator"
validation_key           "#{ENV['HOME']}/.ssh/paasage-cetic-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/paasage-cetic"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
#cookbook_path            ["#{current_dir}/../site-cookbooks"]
cookbook_path            ["site-cookbooks","cookbooks"]
node_path                'nodes'
role_path                'roles'

knife[:berkshelf_path] = "cookbooks"
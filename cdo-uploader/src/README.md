CDO-Uploader
========
Tool for inserting .xmi models to CDO database. 

Configuration
--------
Config file must be located under **~/.paasage** directory:
- **eu.paasage.mddb.cdo.client.properties** - used by the CDO Client repository

Usage
--------
Building:

- **mvn clean install** - creates jar with all needed dependencies (e.g. cdo-uploader-{version}-jar-with-dependencies.jar)

Running:

- **java -jar cdo-uploader-{version}-jar-with-dependencies.jar**

Application will be running and waiting for .xmi models to upload.

Models should be located under: **./models** followed by path which represents path to CDO. e.g:

- File **./models/cpGenerator-operatingSystems.xmi** will be stored in CDO under path: **cpGenerator-operatingSystems**
- File **./models/upperware-models/fms/Amazon.xmi** will be stored in CDO under path: **upperware-models/fms/Amazon**



Running as a XmiToCamel converter

- **java -jar cdo-uploader-{version}-jar-with-dependencies.jar -xmiToCamel -i <PATH_TO_XMI_FILE> -o <PATH_TO_CAMEL_FILE>** 
package eu.melodic.cdo.uploader.camel.service.impl;

import eu.melodic.cdo.uploader.camel.service.CdoService;
import eu.paasage.mddb.cdo.client.CDOClient;
import eu.paasage.upperware.metamodel.cp.CpPackage;
import eu.paasage.upperware.metamodel.types.TypesPackage;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.emf.ecore.EObject;

import java.io.File;

@Slf4j
public class CdoServiceImpl implements CdoService {

    public void storeModelFromFileInCDO(String cdoName, File file) throws Exception {
        try {
            log.info("Storing Model {} into CDO", cdoName);

            CDOClient client = getCdoClient();

            EObject model = CDOClient.loadModel(file.getAbsolutePath());

            client.storeModel(model, cdoName, true);
            log.info("Model {} successfully stored into CDO", cdoName);
            client.closeSession();

        } catch (Exception e) {
            log.error("Could not store model in CDO ", e);
            throw e;
        }
    }

    @Override
    public void camelToXmi(String camelPath, String xmiPath) {
        throw new UnsupportedOperationException("CamelToXmi is not supported so far");
    }

    @Override
    public void xmiToCamel(String xmiPath, String camelPath) {
        log.info("Converting camel {} to xmi {}", camelPath, xmiPath);
//        boolean b = CDOClient.xmiToCAMEL(xmiPath, camelPath);
  //      log.info("Camel converted {}", b);
    }

    @Override
    public String getCdoName(String baseDir, String absolutePath, String fileExtension) {
            return absolutePath
                    .substring(0, absolutePath.indexOf(fileExtension) -1)
                    .substring(baseDir.length() +1);
    }

    private CDOClient getCdoClient() {
        CDOClient client = new CDOClient();
        registrePackages(client);
        return client;
    }

    private void registrePackages(CDOClient cdoClient){
		cdoClient.registerPackage(CpPackage.eINSTANCE);
		cdoClient.registerPackage(TypesPackage.eINSTANCE);
    }

}

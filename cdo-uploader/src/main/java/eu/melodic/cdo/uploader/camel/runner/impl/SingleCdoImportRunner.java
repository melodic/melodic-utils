package eu.melodic.cdo.uploader.camel.runner.impl;

import eu.melodic.cdo.uploader.camel.runner.CdoImportRunner;
import eu.melodic.cdo.uploader.camel.service.CdoService;
import eu.melodic.cdo.uploader.camel.service.impl.CdoServiceImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by pszkup on 24.11.17.
 */
@Slf4j
public class SingleCdoImportRunner implements CdoImportRunner {

    private CdoService cdoService = new CdoServiceImpl();
    private Properties properties;

    public SingleCdoImportRunner() throws IOException {
        properties = loadProperties();
    }

    @Override
    public void run(String path) throws Exception {
        log.info("Storing files from {}", path);
        Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .filter(path1 -> path1.toString().endsWith(properties.getProperty("upload.file.extension")))
                .forEach(path1 -> {
                    String fileName = path1.toString();
                    String cdoName = cdoService.getCdoName(path, path1.toString(), properties.getProperty("upload.file.extension"));
                    try {
                        cdoService.storeModelFromFileInCDO(cdoName, path1.toFile());
                        log.info("File {} stored in CDO under path {}", fileName, cdoName);
                    } catch (Exception e) {
                        log.error("Error during storing file {} under path {}", fileName, cdoName, e);
                    }
                });
        log.info("Storing files from {} ended", path);
    }

    private Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream("/prop.properties"));
        return properties;
    }

}

package eu.melodic.cdo.uploader.camel;

import eu.melodic.cdo.uploader.camel.service.CdoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.component.properties.PropertiesLocation;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class UploadRouteBuilder extends RouteBuilder {

    private CdoService cdoService;

    public UploadRouteBuilder(CdoService cdoService){
        this.cdoService = cdoService;
    }

    @Override
    public void configure() throws Exception {
        final Properties properties = copyProperties();

        from("file://" + CdoService.getModelDir() + "?filterFile=${file:name.ext} ends with '{{upload.file.extension}}'&recursive=true&move=.done/${file:name.noext}-${date:now:yyyyMMddHHmmssSSS}.${file:ext}&moveFailed=.error/${file:name.noext}-${date:now:yyyyMMddHHmmssSSS}.${file:ext}")
                .process(exchange -> {
                    File body = exchange.getIn().getBody(File.class);

                    String absolutePath = body.getAbsolutePath();
                    String cdoName = cdoService.getCdoName(CdoService.getModelDir(), absolutePath, properties.getProperty("upload.file.extension"));
                    log.info("Model: {} will be stored in CDO under path {}", absolutePath, cdoName);
                    exchange.getIn().setHeader("cdoName", cdoName);
                })
                .to("bean:cdoService?method=storeModelFromFileInCDO(${header.cdoName}, ${body})")
                .end();
    }

    private Properties copyProperties() throws IOException {
        PropertiesComponent propertiesComponent = getFromRegistry("properties", PropertiesComponent.class);

        PropertiesLocation location = propertiesComponent.getLocations().get(0);

        Properties result = new Properties();
        result.load(getClass()
                .getClassLoader()
                .getResourceAsStream(location.getPath()));
        return result;
    }

    private <T> T getFromRegistry(String name, Class<T> type) {
        return this.getContext()
                .getRegistry()
                .findByTypeWithName(type)
                .get(name);
    }

}

package eu.melodic.cdo.uploader.camel.runner;

public interface CdoImportRunner {

    void run(String path) throws Exception;
}

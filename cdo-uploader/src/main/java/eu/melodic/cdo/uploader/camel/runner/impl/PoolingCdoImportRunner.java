package eu.melodic.cdo.uploader.camel.runner.impl;

import eu.melodic.cdo.uploader.camel.UploadRouteBuilder;
import eu.melodic.cdo.uploader.camel.runner.CdoImportRunner;
import eu.melodic.cdo.uploader.camel.service.CdoService;
import eu.melodic.cdo.uploader.camel.service.impl.CdoServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.main.Main;
import org.apache.camel.main.MainListenerSupport;
import org.apache.camel.main.MainSupport;

/**
 * Created by pszkup on 24.11.17.
 */
@Slf4j
public class PoolingCdoImportRunner implements CdoImportRunner {

    @Override
    public void run(String path) throws Exception {
        CdoService cdoService = new CdoServiceImpl();

        Main main = new Main();
        main.bind("cdoService", cdoService);
        main.addRouteBuilder(new UploadRouteBuilder(cdoService));
        main.addMainListener(new Events());
        main.bind("properties", new PropertiesComponent("prop.properties"));

        log.info("Starting CdoUploader. Use ctrl + c to terminate the JVM.");
        main.run();
    }

    public static class Events extends MainListenerSupport {

        @Override
        public void afterStart(MainSupport main) {
            log.info("CdoUploader is now started and waiting for models in {}", CdoService.getModelDir());
        }

        @Override
        public void beforeStop(MainSupport main) {
            log.info("CdoUplaoder is now being stopped!");
        }
    }

}

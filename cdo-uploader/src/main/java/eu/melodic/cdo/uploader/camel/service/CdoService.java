package eu.melodic.cdo.uploader.camel.service;

import java.io.File;

public interface CdoService {
    void storeModelFromFileInCDO(String cdoName, File file) throws Exception;

    void camelToXmi(String camelPath, String xmiPath);

    void xmiToCamel(String xmiPath, String camelPath);

    String getCdoName(String baseDir, String absolutePath, String fileExtension);

    static String getModelDir(){
        return System.getProperty("user.dir")+"/models";
    }

}

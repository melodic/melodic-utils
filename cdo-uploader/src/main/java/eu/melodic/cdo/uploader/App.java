package eu.melodic.cdo.uploader;

import eu.melodic.cdo.uploader.camel.runner.impl.PoolingCdoImportRunner;
import eu.melodic.cdo.uploader.camel.runner.impl.SingleCdoImportRunner;
import eu.melodic.cdo.uploader.camel.service.impl.CdoServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

@Slf4j
public class App {

    public static void main(String[] args) throws Exception {
        CommandLine cmd = new DefaultParser().parse(createOptions(), args, true);

        if (cmd.hasOption("camelToXmi")) {
            checkInputOutputOptions(cmd);
            new CdoServiceImpl().camelToXmi(cmd.getOptionValue("i"), cmd.getOptionValue("o"));

        } else if (cmd.hasOption("xmiToCamel")) {
            checkInputOutputOptions(cmd);
            new CdoServiceImpl().xmiToCamel(cmd.getOptionValue("i"), cmd.getOptionValue("o"));

        } else if (cmd.hasOption("mode")) {
            String mode = cmd.getOptionValue("mode");
            if ("pooling".equals(mode)) {
                runAsPooling();
            } else if ("single".equals(mode)) {
                if (cmd.hasOption("path")) {
                    String path = cmd.getOptionValue("path");
                    log.info("Loading files from {}", path);
                    runAsSingle(path);
                } else {
                    log.error("In case of single mode - path option is required");
                }
            } else {
                log.error("Wrong option value '{}'. Possible values: (pooling, single)", mode);
            }
        } else {
            log.info("Missing mode argument - running as pooling component");
            runAsPooling();
        }
    }

    private static void checkInputOutputOptions(CommandLine cmd) throws Exception {
        checkOption(cmd, "i", "Missing -i option");
        checkOption(cmd, "o", "Missing -o option");
    }

    private static void checkOption(CommandLine cmd, String i, String s) throws Exception {
        if (!cmd.hasOption(i)) {
            log.error(s);
            throw new Exception(s);
        }
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption((Option.builder("mode").hasArg(true).desc("Running mode (pooling, single)").build()));
        options.addOption((Option.builder("path").hasArg(true).desc("Path to models dir").build()));

        options.addOption(Option.builder("camelToXmi").hasArg(false).longOpt("camelToXmi").build());
        options.addOption(Option.builder("xmiToCamel").hasArg(false).longOpt("xmiToCamel").build());

        options.addOption(Option.builder("i").hasArg(true).longOpt("input").build());
        options.addOption(Option.builder("o").hasArg(true).longOpt("output").build());
        return options;
    }

    private static void runAsSingle(String path) throws Exception {
        log.info("Runing as single");
        new SingleCdoImportRunner().run(path);
    }

    private static void runAsPooling() throws Exception {
        log.info("Runing as pooling service");
        new PoolingCdoImportRunner().run(null);
    }


}

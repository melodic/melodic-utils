package eu.paasage.mddb.cdo.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public class MyIOUtils {
	
	private static final Logger logger = org.apache.log4j.Logger.getLogger(MyIOUtils.class);
	
	private static final int BUFFER = 2048;
	private static int id = 1;

	
	public static boolean createZipArchive(String srcFolder, String suffix, boolean subDirs, OutputStream output) {

	    try {
	        BufferedInputStream origin = null;

	        //FileOutputStream  output = new FileOutputStream(new File(srcFolder+ ".zip"));

	        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(output));
	        byte data[] = new byte[BUFFER];

	        File subDir = new File(srcFolder);
	        File subdirList[] = subDir.listFiles();
	        for(File f:subdirList)
	        {
	                // get a list of files from current directory
	                //File f = new File(srcFolder + File.separator + sd);
	                if(f.isDirectory())
	                {
	                	if (subDirs){
		                    File files[] = f.listFiles();
	
		                    for (int i = 0; i < files.length; i++) {
		                        logger.debug("Adding: " + files[i].getName());
		                        FileInputStream fi = new FileInputStream(files[i]);
		                        origin = new BufferedInputStream(fi, BUFFER);
		                        ZipEntry entry = new ZipEntry(f.getName() + File.separator + files[i].getName());
		                        out.putNextEntry(entry);
		                        int count;
		                        while ((count = origin.read(data, 0, BUFFER)) != -1) {
		                            out.write(data, 0, count);
		                            //out.flush();
		                        }
		                        out.closeEntry();
		                        origin.close();
	
		                    }
	                	}
	                }
	                else //it is just a file
	                {
	                	if (!f.getName().endsWith(suffix)) continue;
	                	logger.debug("Adding: " + f.getName() + " canRead:" + f.canRead());
	                    FileInputStream fi = new FileInputStream(f);
	                    origin = new BufferedInputStream(fi, BUFFER);
	                    ZipEntry entry = new ZipEntry(f.getName());
	                    out.putNextEntry(entry);
	                    int count;
	                    int totalCount = 0;
	                    while ((count = origin.read(data, 0, BUFFER)) != -1) {
	                        out.write(data, 0, count);
	                        //out.flush();
	                        totalCount = totalCount + count;
	                        logger.debug("Read inc. " + count + " bytes for: " + f.getName());
	                    }
	                    logger.debug("Read totally " + totalCount + " bytes for: " + f.getName());
	                    entry.setSize(totalCount);
	                    out.closeEntry();
	                    origin.close();

	                }
	        }
	        //out.flush();
	        out.finish();
	        out.close();
	    } catch (Exception e) {
	    	logger.error("", e);
	        logger.debug("createZipArchive threw exception: " + e.getMessage());        
	        return false;

	    }


	    return true;
	} 
	
	public static boolean deleteFiles(String folder, String suffix){
		File dir = new File(folder);
        File dirList[] = dir.listFiles();
        try{
	        for(File f:dirList)
	        {
	        	if (f.getName().endsWith(suffix)){
	        		f.delete();
	        	}
	        }
	        return true;
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        return false;
	}
	
	public static void loadInputStream(File f, InputStream is){
		BufferedReader bis = new BufferedReader(new InputStreamReader(is));
		try{
			PrintWriter dos = new PrintWriter(new FileOutputStream(f));
			String s = bis.readLine();
			while (s != null){
				dos.println(s);
				s = bis.readLine();
			}
			dos.flush();
			dos.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}

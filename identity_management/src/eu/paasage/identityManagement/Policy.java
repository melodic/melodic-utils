package eu.paasage.identityManagement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;

import javax.swing.JOptionPane;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

public class Policy {

	private static	String user = "root";
	private static String pass = "i5RD0oy6";


	public static final String JAVABRIDGE_PORT="8080";
	static final php.java.bridge.JavaBridgeRunner runner =
			php.java.bridge.JavaBridgeRunner.getInstance(JAVABRIDGE_PORT);

	private static Connection connect = null;
	private static Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private static ResultSet resultSet = null;




	public static String getPolicy(String uri) throws Exception{

		String organisation = "AGHOrganiation";
		cdoPolicyManager cdoPM = new cdoPolicyManager();
		//if security level is high output is true
		boolean secLevHigh = cdoPM.checkSecurityLevel(organisation);
		System.out.println(" IS SEC LEL HIGH? = " + secLevHigh);
		String out = getAssignedRole(uri);
		String[] split = out.split(":");
		String  subject = split[0];
		subject = subject.substring((subject.lastIndexOf('[') +1), subject.length());
		subject = subject.trim();
		System.out.println(" username = " + user);
		String  role = split[1];
		role = role.substring(1, role.lastIndexOf(']'));
		System.out.println(" role = " + role);
		String actions = getActionsByRole(role);
		String[] split2 = actions.split(":");
		String actionId = split2[1].replace(']',' ');
		actionId = actionId.replace('=',' ').trim();
		System.out.println(" action = " + actionId);
		String action = getAssignedAction(actionId);
		action = action.trim();
		System.out.println(" action = " + action);
		if (secLevHigh = true){
			action = "Read";
		}
		String[] input = new String[4];
		input[0]= action;
		input[1]= uri;
		input[2]= subject;
		input[3]= role;
		String policy = invokeCreate(input);
		System.out.println("policy created = " + policy);




		return policy;


	}

	public static String getAssignedAction(String id) throws Exception {
		String action = null; 
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			// Setup the connection with the DB
			connect = DriverManager.getConnection("jdbc:mysql://kirkhac3.miniserver.com:3306/userDB", user, pass);


			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// Result set get the result of the SQL query
			System.out.println(" before select");
			resultSet = statement
					.executeQuery("select * from  Action where actionId ='"+ id +"'");

			while (resultSet.next())
			{
				System.out.println(" in the loop");
				//   String userName = resultSet.getString("username");

				action = resultSet.getString("name");      

			}

		} catch (Exception e) {
			System.out.println("error in SQL " + e);
		}
		connect.close();	    


		return action; 

	}

	//if from remote i.e. php
	public static String invokeCreate(String[] input) throws Exception{
		System.out.println("in invokeCreate ");

		String action = input[0];
		String target = input[1];
		String subject = input[2];
		String resource = input[3];
		System.out.println("in res ");	
		String res = prime(action, target, subject, resource);
		System.out.println("res = " + res);
		//	policyCheckService pcs = new policyCheckService();
		//	System.out.println("args 4 = " + args[4]);
		//	String output = pcs.check(args[4], subject, resource, action);
		//	System.out.println("done! and here is the result " + output);


		return res;
	}


	public static String getAssignedRole(String uri) throws Exception {
		ArrayList<String> a = new ArrayList<String>();
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			// Setup the connection with the DB
			connect = DriverManager.getConnection("jdbc:mysql://kirkhac3.miniserver.com:3306/userDB", user, pass);


			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// Result set get the result of the SQL query
			System.out.println(" before select");
			resultSet = statement
					.executeQuery("select * from  AssignRole where CTPID ='"+ uri +"'");

			while (resultSet.next())
			{
				System.out.println(" in the loop");
				//   String userName = resultSet.getString("username");

				String userName = resultSet.getString("username");      
				String role = resultSet.getString("roleId");
				a.add(userName + " := " + role);

				//  String target = resultSet.getString("CTPID");
			}

		} catch (Exception e) {
			System.out.println("error in SQL " + e);
		}
		connect.close();

		return a.toString(); 

	}
	public static String getActionsByRole(String id) throws Exception {
		ArrayList<String> a = new ArrayList<String>();
		System.out.println(" in getActions and actionId = " + id);
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

			// Setup the connection with the DB
			connect = DriverManager.getConnection("jdbc:mysql://kirkhac3.miniserver.com:3306/userDB", user, pass);


			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// Result set get the result of the SQL query
			System.out.println(" before select");
			resultSet = statement
					.executeQuery("select * from  AvailableActionByRole where roleId =' " + id + "'");

			while (resultSet.next())
			{

				//   String userName = resultSet.getString("username");

				String role = resultSet.getString("roleId");      
				String actionId = resultSet.getString("actionId");    
				a.add(role + " := " + actionId);
				//  String target = resultSet.getString("CTPID");
			}

		} catch (Exception e) {
			System.out.println("error in SQL " + e);
		}
		connect.close();

		return a.toString(); 

	}



	public static String invokeCheck(String invoke[], String policyFile) throws Exception{
		String output = null;    
		String action = invoke[0];
		String target = invoke[1];
		String subject = invoke[2];
		String resource = invoke[1];

		PolicyCheck pc = new PolicyCheck();
		output = pc.check(policyFile, subject, resource, action);


		return output;
	}

	public static String prime(String action, String target, String subject, String resource) throws FileNotFoundException{
		//create psudo file for policy Read
		String result = null;
		String psudpolicy = "PolicySet\n"+ 
				"Target\n"+
				"   TA (act-id, \""+ action +"\")\n"+
				" Policy\n"+
				"   Target\n"+
				"   Rules(Policy)\n"+
				"    Rule (ef:Permit)\n"+
				"     Target\n"+
				"      TS (sub-id,\"" + subject + "\")\n"+
				"      TR (res-id,\"" + target + ".*\" mtId:rgx-mt)\n"+
				"    Rule (ef:Deny)\n"+
				"     Target"; 
		System.out.println(" ............. " + psudpolicy + " ................. ");

		String policy = write(psudpolicy, target);
		return policy;




	}

	public static String write(String input, String id){

		FileOutputStream fop = null;
		File file;
		String policy = id + ".xml";
		try {

			file = new File(policy);
			fop = new FileOutputStream(file);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			// get the content in bytes
			byte[] contentInBytes = input.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return policy;
		}
	}
}

